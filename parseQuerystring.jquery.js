jQuery.extend({
  parseQuerystring: function(){
    var nvpair = {};
    var qs = window.location.search.replace('?', '');
	/*
	
	NOTE:
	If the query comes after the hashtag (e.g. index.php#main?id=4), window.location.search will return blank, and the query string appears as part of window.location.hash instead. While it is unconventional to find the query string after the hashtag, I've yet to find a browser that has a problem with it, so I'm expanding the plugin to accommodate this. 
	
	*/
	if(!qs.length){
		var h = window.location.hash.match(/\?.*/);
		if(h != null && h.length){
			qs = h.toString().replace('?','');			
		}
	}
    var pairs = qs.split('&');
    $.each(pairs, function(i, v){
      var pair = v.split('=');
      nvpair[pair[0]] = pair[1];
    });
    return nvpair;
  }
});